
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login</title>
<link rel="stylesheet" type="text/css" href="../CSS/style.css">
</head>
<body>

	<header>
	<div class="container">
		<img src="../Img/logo.png" class="logo">

		<nav>
		<ul>
			<li><a href="about.jsp">About</a></li>
			<li><a href="contact.jsp">Contact</a></li>
			<br>
			<br>

		</ul>
		</nav>
	</div>
	</header>

	<div class="login-box">
		<img src="../Img/avatar.png" class="avatar">
		<h1>Registration</h1>
		<form action="../UserRegistration" method="post">
			<p>Email</p>
			<input type="text" name="mailAddress" placeholder="Enter Email"
				required="required">
			<p>Name</p>
			<input type="text" name="userName" placeholder="Enter User Name"
				required="required">
			<p>Password</p>
			<input type="password" name="password" placeholder="Enter Password"
				required="required">
			<p>Phone Number</p>
			<input type="text" name="phoneNumber"
				placeholder="Enter Phone Number" required="required"> <input
				type="submit" value="REGISTER">
		</form>
		<c:out value="${registrationStatus}" />
	</div>
</body>
</html>


