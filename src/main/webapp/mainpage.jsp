<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>mainpage</title>
<link rel="stylesheet" href="CSS/style.css">
</head>
<body>

	<header>
	<div class="container">
		<img src="Img/logo.png" class="logo">

		<nav>
		<ul>

			<li><a href="JSP/about.jsp">About</a></li>
			<li><a href="JSP/contact.jsp">Contact</a></li>
			<br>
			<br>

		</ul>
		</nav>
	</div>
	</header>

	<div class="login-box">

		<img src="Img/avatar.png" class="avatar">
		<h1>Login</h1>
		<br> <br>
		<form action="LoginUser" method="post">
			<p>Email</p>
			<input type="text" name="mailAddress" placeholder="Enter Email"
				required="required"> <br>
			<p>Password</p>
			<input type="password" name="password" placeholder="Enter Password"
				required="required"> <br>
			<br>
			<br>
			<br> <input type="submit" name="submit" value="Login">
			Don't have an Account? Click <a href="JSP/user-registration.jsp"><font
				color="#f4d804"> here </font></a> to Sing Up.
		</form>
		<br>
		<c:set var="user" scope="session" value="${user}" />
		<c:if test="${not empty user}">
			<c:redirect url="JSP/ordertaxi.jsp"></c:redirect>
		</c:if>
		<!-- dodac tutaj ifa elsa w JSTL że zależnie od tego oc w loginstatus -->
		<c:out value="${loginStatus}" />
		<c:out value="${registrationStatus}" />
	</div>
</body>
</html>

