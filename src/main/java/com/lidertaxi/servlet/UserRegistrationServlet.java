package com.lidertaxi.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.lidertaxi.dao.UserDao;
import com.lidertaxi.dao.UserDaoImpl;
import com.lidertaxi.error.UserAccountExistException;
import com.lidertaxi.mail.SendRegistrationMail;
import com.lidertaxi.mail.SendingMail;
import com.lidertaxi.model.User;

@WebServlet(name = "/UserRegistrationServlet", urlPatterns = { "/UserRegistration" })
public class UserRegistrationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public UserRegistrationServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		String mailAddress = request.getParameter("mailAddress");
		String password = request.getParameter("password");
		String userName = request.getParameter("userName");
		String phoneNumber = request.getParameter("phoneNumber");
		HttpSession session = request.getSession(true);
		session.setMaxInactiveInterval(1);
		UserDao dao = new UserDaoImpl();
		User user = null;
		boolean result = false;
		user = new User(mailAddress, userName, password, phoneNumber);
		try {
			result = dao.saveUser(user);
			System.out.println(result);
		} catch (UserAccountExistException e) {
			session.setAttribute("registrationStatus", "cantCreateAccountUserAlreadyExists");
			response.sendRedirect("JSP/user-registration.jsp");
		}
		if (user != null && result) {
			String userMailAddress = user.getMail_address(); // to wątku inaczej nie działą cos
			SendingMail sendRegistrationMail = new SendRegistrationMail();
			new Thread(new Runnable() {
				public void run() {
					sendRegistrationMail.sendMail(userMailAddress, SendingMail.LIDER_TAXI_ADDRESS);
				}
			}).start();
			session.setAttribute("registrationStatus", "createAccount");
			response.sendRedirect("mainpage.jsp");
			System.out.println("Sending mail correctly");
		}
	}

}
