package com.lidertaxi.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class LogoutUserServlet
 */
@WebServlet(name = "/LogoutUserServlet", urlPatterns = { "/LogoutUser" })
public class LogoutUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public LogoutUserServlet() {
        super();
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		if(session!=null) {
			session.removeAttribute("user");
		}
		session.setAttribute("loginStatus", "successLogout");
		response.sendRedirect("mainpage.jsp");
		session.setMaxInactiveInterval(1);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
