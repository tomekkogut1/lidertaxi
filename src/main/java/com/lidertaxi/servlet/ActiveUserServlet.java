package com.lidertaxi.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.lidertaxi.dao.UserDao;
import com.lidertaxi.dao.UserDaoImpl;
import com.lidertaxi.model.UserStatus;

@WebServlet(name = "/ActiveUserServlet", urlPatterns = { "/ActiveUser" })
public class ActiveUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public ActiveUserServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String login = request.getParameter("login");
		UserDao dao = new UserDaoImpl();
		boolean gate = dao.updateStatus(login, UserStatus.ACTIVATED);
		if (gate == true) {
			RequestDispatcher rd = request.getRequestDispatcher("JSP/successactivation.jsp");
			rd.include(request, response);
		} else {
			HttpSession session = request.getSession(true);
			session.setMaxInactiveInterval(1);
			session.setAttribute("registrationStatus", "yourAccountNotExists");
			response.sendRedirect("JSP/user-registration.jsp");
			System.out.println("can not update status to ACTIVATED");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

}
