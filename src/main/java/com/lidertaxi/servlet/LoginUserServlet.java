package com.lidertaxi.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.lidertaxi.dao.UserDao;
import com.lidertaxi.dao.UserDaoImpl;
import com.lidertaxi.model.User;

@WebServlet(name = "/LoginUserServlet", urlPatterns = { "/LoginUser" })
public class LoginUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public LoginUserServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		String mailAddress = request.getParameter("mailAddress");
		String password = request.getParameter("password");
		UserDao dao = new UserDaoImpl();
		HttpSession session = request.getSession(true);
		session.setMaxInactiveInterval(1);
		User user = null;
		user = dao.getUserByLoginData(mailAddress, password);
		if (user != null) {
			session.setAttribute("user", user);
			session.setMaxInactiveInterval(60 * 60); // one hour
			response.sendRedirect("JSP/ordertaxi.jsp");
		} else {
			session.setAttribute("loginStatus", "incorrectLogin");
			response.sendRedirect("mainpage.jsp");
		}
	}

}
