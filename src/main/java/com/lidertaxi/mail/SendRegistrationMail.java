package com.lidertaxi.mail;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SendRegistrationMail implements SendingMail {

	@Override
	public boolean sendMail(String reciverMail, String senderMail) {
		boolean result = false;
		String to = "t.kogut96@gmail.com";
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", HOST);
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(LIDER_TAXI_USER_NAME, LIDER_TAXI_PASSWORD);
			}
		});

		try {
			String href = "<a target =\"_blank\" href=\"http://localhost:8181/LiderTaxi-app/ActiveUser" + "?login="
					+ reciverMail + "\">http://localhost:8181/LiderTaxi-app/ActiveUser</a>";
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(LIDER_TAXI_ADDRESS));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
			message.setSubject("[LiderTaxi] Please verify your mail address");
			message.setContent(
					"Hey, <br>" + reciverMail + "<br>" + "Visit this link to verify your email address:" + "<h3>" + href
							+ "</h3>" + "Please do not reply to this notification, this inbox is not monitored.<br>"
							+ "If you are having a problem with your account, please email lidertaxi7@gmail.com"
							+"<br>This link will be active in 24 hours after sending. "
							+ "<br><br>" + "Thanks for using the site!",
					"text/html");
			Transport.send(message);
			result = true;
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
		return result;

	}

}
