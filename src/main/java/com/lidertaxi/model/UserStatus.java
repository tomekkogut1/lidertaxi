package com.lidertaxi.model;

public enum UserStatus {
	BAN, PREMIUM, ACTIVATED, NONACTIVATED;
}
