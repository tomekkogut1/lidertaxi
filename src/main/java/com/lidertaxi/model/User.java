package com.lidertaxi.model;

public class User {

	private String mail_address;
	private String name;
	private String password;
	private String phone_number;
	private int priority;
	private UserStatus status;
	public String getMail_address() {
		return mail_address;
	}
	public void setMail_address(String mail_address) {
		this.mail_address = mail_address;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPhone_number() {
		return phone_number;
	}
	public void setPhone_number(String phone_number) {
		this.phone_number = phone_number;
	}
	public int getPriority() {
		return priority;
	}
	public void setPriority(int priority) {
		this.priority = priority;
	}
	public UserStatus getStatus() {
		return status;
	}
	public void setStatus(UserStatus status) {
		this.status = status;
	}

	public User(){}
	public User(String mail_address, String name, String password, String phone_number) {
		super();
		this.mail_address = mail_address;
		this.name = name;
		this.password = password;
		this.phone_number = phone_number;
	}
	public User(String mail_address, String name, String password, String phone_number, int priority, UserStatus status) {
		this(mail_address, name , password, phone_number);
		this.priority = priority;
		this.status = status;
	};

}
