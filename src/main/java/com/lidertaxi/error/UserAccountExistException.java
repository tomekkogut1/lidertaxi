package com.lidertaxi.error;

public class UserAccountExistException extends Exception{
	private static final long serialVersionUID = 1L;
	public UserAccountExistException(String message) {
		super(message);
	}
}
