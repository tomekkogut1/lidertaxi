package com.lidertaxi.error;

public class BanAccountException extends Exception{
	private static final long serialVersionUID = 1L;
	public BanAccountException(String message) {
		super(message);
	}
}
