package com.lidertaxi.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.lidertaxi.error.UserAccountExistException;
import com.lidertaxi.model.User;
import com.lidertaxi.model.UserStatus;
import com.lidertaxi.util.ConnectionProvider;

public class UserDaoImpl implements UserDao {
	@Override
	public User getUserByLoginData(String mailAddress, String password) {
		User user = null;
		Connection conn = null;
		PreparedStatement prepStmt = null;
		ResultSet resultSet = null;
		try {
			conn = ConnectionProvider.getConnection();
			prepStmt = conn.prepareStatement(READ);
			prepStmt.setString(1, mailAddress);
			prepStmt.setString(2, password);
			prepStmt.setString(3, UserStatus.ACTIVATED.name());
			resultSet = prepStmt.executeQuery();
			if (resultSet.next()) {
				String name = resultSet.getString("name");
				String phone_number = resultSet.getString("phone_number");
				UserStatus status = UserStatus.valueOf(resultSet.getString("status"));
				int priority = resultSet.getInt("priority");
				user = new User(mailAddress, name, password, phone_number, priority, status);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			releaseResources(prepStmt, resultSet, conn);
		}
		return user;
	}

	@Override
	public boolean saveUser(User user) throws UserAccountExistException {
		boolean result = false;
		Connection conn = null;
		PreparedStatement prepStmt = null;
		try {
			if (!accountExist(user.getMail_address())) {
				conn = ConnectionProvider.getConnection();
				prepStmt = conn.prepareStatement(CREATE);
				prepStmt.setString(1, user.getMail_address());
				prepStmt.setString(2, user.getName());
				prepStmt.setString(3, user.getPassword());
				prepStmt.setString(4, user.getPhone_number());
				int rowsAffected = 0;
				rowsAffected = prepStmt.executeUpdate();
				if (rowsAffected == 1) {
					result = true;
				}
			} else
				throw new UserAccountExistException("User account exist");

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			releaseResources(prepStmt, null, conn);
		}
		return result;
	}

	@Override
	public boolean updateStatus(String mailAddress, UserStatus status) {
		boolean result = false;
		Connection conn = null;
		PreparedStatement prepStmt = null;
		try {
			conn = ConnectionProvider.getConnection();
			prepStmt = conn.prepareStatement(UPDATE_STATUS);
			prepStmt.setString(1, UserStatus.ACTIVATED.name());
			prepStmt.setString(2, mailAddress);
			int rowsAffected = prepStmt.executeUpdate();
			if (rowsAffected == 1) {
				result = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			releaseResources(prepStmt, null, conn);
		}
		return result;
	}

	private boolean accountExist(String mailAddress) {
		boolean result = false;
		Connection conn = null;
		PreparedStatement prepStmt = null;
		ResultSet resultSet = null;
		try {
			conn = ConnectionProvider.getConnection();
			prepStmt = conn.prepareStatement("SELECT mail_address FROM users WHERE mail_address=?;");
			prepStmt.setString(1, mailAddress);
			resultSet = prepStmt.executeQuery();
			if (resultSet.next()) {
				result = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			releaseResources(prepStmt, resultSet, conn);
		}
		return result;
	}

	private void releaseResources(PreparedStatement prepStmt, ResultSet res, Connection conn) {
		try {
			if (prepStmt != null && !prepStmt.isClosed()) {
				prepStmt.close();
			}
			if (res != null && !res.isClosed()) {
				res.close();
			}
			if (conn != null && !conn.isClosed()) {
				conn.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
