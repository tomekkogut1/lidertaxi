package com.lidertaxi.dao;

import com.lidertaxi.error.UserAccountExistException;
import com.lidertaxi.model.User;
import com.lidertaxi.model.UserStatus;


public interface UserDao {
	final static String CREATE = "INSERT INTO users(mail_address, name, password, phone_number) VALUES(?, ?, ?, ?);";
	final static String READ = "SELECT * FROM users WHERE mail_address=? and password =? and status = ?;";
	final static String UPDATE_STATUS = "UPDATE users SET status=? WHERE mail_address = ?;";
	final static String DELETE = "DELETE FROM users WHERE mail_address=?;";
	User getUserByLoginData(String mailAddress, String password);
	boolean saveUser(User user) throws UserAccountExistException;
	boolean updateStatus(String mailAddress , UserStatus status);
}
